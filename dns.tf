resource "aws_route53_zone" "unat-edu-pe" {
  provider = aws.focusinlab
  name     = "666.edu.pe"
}

resource "aws_route53_record" "sample-unat-edu-pe" {
  provider = aws.focusinlab
  name     = "sample"
  type     = "A"
  zone_id  = aws_route53_zone.unat-edu-pe.zone_id

  records = [
    aws_instance.example.public_ip
  ]

  ttl = "300"
}
