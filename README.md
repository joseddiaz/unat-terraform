# UNAT - Terraform IAC

POC: Infrastructure as code for UNAT

## Pre-requisites

- You must have [Terraform](https://www.terraform.io/) installed on your computer.
- You must have an [Amazon Web Services (AWS) account](http://aws.amazon.com/).

Please note that this code was written for Terraform 0.12.x.

## Quick start

Configure your [AWS access
keys](https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_credentials_profiles.html) as a new AWS Profile, In order to do that add the following line inside your `.aws/credentials` file:

```
...
[focusinlab]
aws_access_key_id=(your access key id)
aws_secret_access_key=(your secret access key)
```

Deploy the code:

```
terraform init
terraform apply
```

When the `apply` command completes, it will output the address and port for RDS, and the EC2 Public IP

Clean up when you're done:

```
terraform destroy
```
