provider "aws" {
  region  = "us-east-2"
  profile = "focusinlab"
  alias   = "focusinlab"
}
