resource "aws_db_instance" "example" {
  provider            = aws.focusinlab
  identifier_prefix   = "example-database"
  engine              = "mysql"
  allocated_storage   = 10
  instance_class      = "db.t2.micro"
  name                = "sample"
  username            = "admin"
  password            = "P_ssw0rd" # Use vars instead
  skip_final_snapshot = true
}
